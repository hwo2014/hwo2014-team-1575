/*
 */
package Javatars;

import java.io.PrintWriter;

/**
 *
 * @author rothens
 */
public class Logger {

    private PrintWriter writer;

    public Logger(String filename) {
        try {
            writer = new PrintWriter(filename);
        } catch (Exception ignored) {
        }
    }

    public void write(String text) {
        if (writer != null) {
            writer.print(text);
        }
    }
    
    public void close(){
        if (writer != null) {
            writer.close();
        }
    }
}
