package Javatars;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import java.util.HashMap;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    Brain brain = new Brain(this);
    

    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            /*if(msgFromServer.data != null){
                System.out.println(msgFromServer.msgType);
                System.out.println(msgFromServer.data.toString());
            }*/
            SendMsg response;
            response = this.brain.process(msgFromServer);
            if (response != null) {
                send(response);
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    public void sendExtramsg (final SendMsg msg) {
           this.send(msg);
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}
class SwitchLane extends SendMsg {
    public final String data;

    public SwitchLane(String data) {
        this.data = data;
    }
    
    @Override
    protected String msgType() {
        return "switchLane";
    }
    
}

class Turbo extends SendMsg {    
    @Override
    protected String msgType() {
        return "turbo";
    }
    @Override
    protected  Object msgData()  {
        return "hey, see this turbo?";
    } 
}

class JoinRace extends SendMsg {
    public final HashMap<String,String> botId = new HashMap();
    public final String trackName;

    JoinRace(final String name, final String key) {
        botId.put("name", name);
        botId.put("key", key);
        trackName = "keimola";
    }  

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}