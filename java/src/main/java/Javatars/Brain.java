package Javatars;

import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import static java.lang.Double.*;
import static java.lang.Math.*;
import java.util.Map;

/**
 *
 * @author deathowl
 */
public class Brain {
    private static final boolean DEBUG = true;
    private Logger logger = null;
    
    Integer criticalAngle;
    Double lastAngle;
    Double lastSpeed;
    Double lastDiff;
    ArrayList<LinkedTreeMap> track;
    HashMap<Integer, Integer> mapping;
    Double lastPos;
    Main main = null;
    Boolean turboAvailable = false;

    public Brain(Main main) {
        this.lastAngle = 0.0;
        this.lastSpeed = 1.0;
        this.lastPos = 0.0;
        this.lastDiff = 0.0;
        mapping = new HashMap<>();
        this.main = main;
    }

    public SendMsg process(MsgWrapper msgFromServer) {
        switch (msgFromServer.msgType) {
            case "carPositions":
                Double throttle = this.calculateThrottle(msgFromServer.data);
                return (new Throttle(throttle));
            case "join":
                System.out.println("Joined");
                break;
            case "turboAvailable":
                this.turboAvailable = true;
                break;
            case "gameInit":
                System.out.println("Race init");
                LinkedTreeMap initialData = (LinkedTreeMap) msgFromServer.data;
                //This looks bad, right?
                ArrayList pieces = (ArrayList) ((LinkedTreeMap) ((LinkedTreeMap) initialData.get("race")).get("track")).get("pieces");
                this.track = pieces;
                this.postProcessTrack();
                break;
            case "gameEnd":
                System.out.println("Race end");
                if(false && logger != null){
                    logger.close();
                }
            case "gameStart":
                System.out.println("Race start");
                if(false) {
                    logger = new Logger("./log.txt");
                }
                break;
            case "crash":
                System.out.println("I crashed. :(");
                break;
            default:
                return (new Ping());
        }
        return null;
    }

    private Double calculateThrottle(Object msgFromServer) {
         ArrayList<LinkedTreeMap> arl = (ArrayList<LinkedTreeMap>)msgFromServer;
        LinkedTreeMap ourCarState = null;
        for( LinkedTreeMap lkm : arl) {
            if (((LinkedTreeMap)lkm.get("id")).get("name").toString().equals("Javatars")) {
                ourCarState = lkm;
            }
        }
        Double angle = Double.parseDouble(ourCarState.get("angle").toString());
        Double piecepos = Double.parseDouble(((LinkedTreeMap)((LinkedTreeMap)ourCarState).get("piecePosition")).get("pieceIndex").toString());
        Double inpiecepos = Double.parseDouble(((LinkedTreeMap)((LinkedTreeMap)ourCarState).get("piecePosition")).get("inPieceDistance").toString());
        Double currentSpeed = this.calculateTravellingSpeed(this.lastPos, inpiecepos, piecepos.intValue());
        Double diff = Math.abs(Math.abs(this.lastAngle) - Math.abs(angle));
        Double throttle = this.lastSpeed;   
        System.out.println("Diff"+ diff);
        Double currentPieceLen;
        currentPieceLen = this.calculateLengthOfPiece(piecepos.intValue());
        double RemainsFromPiece = currentPieceLen - inpiecepos; 
        if (this.isStraightPiece(piecepos.intValue())) {
            if (currentSpeed > 5.8 
                    &&  this.isImpendingDanger(piecepos.intValue())) {
                if (throttle >= 0.2) {
                    throttle -= 0.1;                    
             }           
           } else {
                throttle = 1.0;
           }
            
        } else {
            if (this.areWeAfterApex(RemainsFromPiece, currentPieceLen)) {
                  if (throttle < 0.9 ) {
                    throttle += 0.2;
                  }
            } else if((currentSpeed > 5.2 && (Math.abs(this.lastAngle) < Math.abs(angle))) || (diff > 4.0 && diff > lastDiff)) {
                    if (throttle > 0.1) {
                        throttle -= 0.2;
                    }
                    
            } else {
                  if (throttle < 1.0 ) {
                    throttle += 0.1;
                  }
            }
            
            if (this.canBeConsideredHarmless(piecepos.intValue()) && !this.isImpendingDanger(piecepos.intValue())) {
                throttle = Double.max(1.0,throttle);
            }
            
             if (currentSpeed > 3  && this.isImpendingDanger(piecepos.intValue())) {
                if (throttle >= 0.2) {
                    throttle -= 0.1;                    
                    }      
                 }
              
            
        }
        if (currentSpeed <= 3.0) { //fuck this.
            throttle = Double.max(0.4,throttle);
        }
        
        if (this.countTurns() > 8 && this.countHarmlessTurns() < 5) {
            throttle = Double.min(0.51, throttle);
        }
        
        if (this.countTurns() <= 4 && this.countHarmlessTurns() == this.countTurns()) {
             throttle = Double.max(1.0, throttle);
        }
        
        if (currentSpeed > 10 && currentSpeed < 20) {
            throttle = 0.5;
        }
        
        if (throttle < 0) {
            throttle = 0.0;
        }
        
         if (throttle > 1) {
            throttle = 1.0;
        }

        this.lastSpeed = throttle;
        this.lastAngle = angle;
        this.lastPos = inpiecepos;
        this.lastDiff = diff;
        this.turboIfWeCan(piecepos.intValue());
        System.out.printf("Current Speed: %f\n", currentSpeed);
        System.out.printf("Throttle: %f%% \n", (throttle / 1.0) * 100);
        if(DEBUG && logger != null){
            logger.write(String.format(Locale.UK, "angle:%f,speed:%f,throttle:%f\n", angle,currentSpeed,throttle));
        }
        return throttle;
    }

    /*
     UTILITY FUNCTIONS
     */
    private double calculateTravellingSpeed(double prevPos, double currPos, int idx) {
        if (currPos < prevPos) { // can occur on trackpart change.
            if (idx > 0) {
                Double prevLen = this.calculateLengthOfPiece(idx - 1);
                return (prevLen - prevPos) + currPos;
            } else {
                return currPos;
            }

        } else { //occurs every other time :)
            return (currPos - prevPos);
        }
    }
    
    
    private void turboIfWeCan(Integer idx) {
        if (this.getDirectionOfNextTurn(idx) == 2 && this.turboAvailable) { // NO more turns in map.
            Turbo turbo = new Turbo();
            System.out.println(turbo.toJson());
            this.main.sendExtramsg(turbo);
            this.turboAvailable = false;
        }
    }

    private Boolean isStraightPiece(Integer idx) {
        if (((LinkedTreeMap) this.getMapPiece(idx)).get("angle") == null) {
            return true;
        }
        return false;
    }
    
    private Boolean canBeConsideredHarmless(Integer idx) {
        return (abs((Double)this.getMapPiece(idx).get("angle"))<= 90.0 
                && (Double)this.getMapPiece(idx).get("radius") > 120.0);
    }
    
    private Boolean canBeConsideredHarmlessDirect(Integer idx) {
        return (abs((Double)this.track.get(idx).get("angle"))<= 90.0 
                && (Double)this.track.get(idx).get("radius") > 120.0);
    }


    private boolean isImpendingDanger(int idx) {
        LinkedTreeMap nextPiece;
        try {
            nextPiece = (LinkedTreeMap) this.getMapPiece(idx + 1);
        } catch (Exception e) {
            return false;
        }
        if (nextPiece.get("angle") == null || this.canBeConsideredHarmless(idx + 1)) {
            return false;
        }
        return true;
    }

    private boolean areWeAfterApex(double RemainsFromPiece, double currentPieceLen) {
        return RemainsFromPiece <= currentPieceLen * 0.5;
    }

    private Double calculateLengthOfPiece(int idx) {
        if (((LinkedTreeMap) this.getMapPiece(idx)).get("angle") == null) {
            return (Double) ((LinkedTreeMap) this.getMapPiece(idx)).get("length");
        } else {
            Double Angle = (Double) ((LinkedTreeMap) this.getMapPiece(idx)).get("angle");
            Double Radius = (Double) ((LinkedTreeMap) this.getMapPiece(idx)).get("radius");
            Double Percentage = abs(Angle) / 360;
            return (2 * PI * Radius) * Percentage;
        }
    }


    private void postProcessTrack() { 
        //FIXME : iterate two times, eliminate multiple 90s
        ArrayList<LinkedTreeMap> tmp = new ArrayList<LinkedTreeMap>();
        LinkedTreeMap last = null;
        LinkedTreeMap original = null;
        Double originalAngle = 0.0;
        Double originalRadius = 0.0;
        Integer lastIdx = 0;
        Integer MappedIdx = 0;
        System.out.println(this.track);
        for (int idx = 0 ; idx< this.track.size(); idx++) {
            LinkedTreeMap trackPiece = this.track.get(idx);
            int j = idx + 1;
            last = trackPiece;
            if (trackPiece.get("angle") != null) {
             
                originalAngle = (Double) trackPiece.get("angle"); // Please no stupid references.
                originalRadius = (Double) trackPiece.get("radius");
                System.out.println(j);
                while (j < this.track.size() && this.track.get(j).get("angle") != null &&
                        ( abs(originalAngle) == abs((Double) this.track.get(j).get("angle"))) &&
                        ( abs(originalRadius) == abs((Double) this.track.get(j).get("radius")))
                ) {
                Double lastangle = abs((Double) last.get("angle"));
                Double currAngle = abs((Double) this.track.get(j).get("angle"));
                Double sum = 0.0;
                if (originalAngle < 0) {
                    sum = - (lastangle + currAngle);
                } else {
                    sum = lastangle + currAngle;
                }
                last.remove("angle");
                last.put("angle", sum);
                this.mapping.put(idx ,MappedIdx);
                j++;
                idx++;
                }
                this.mapping.put(idx ,MappedIdx);
                MappedIdx++;
                System.out.println(last);
                System.out.println();
                tmp.add(last);
            } else {
                this.mapping.put(idx ,MappedIdx);
                MappedIdx++;
                System.out.println(last);
                System.out.println();
                tmp.add(last);
            }
        }
        this.track = tmp;
        System.out.println(this.track);
        System.out.println(this.mapping);
    }
    
    private Integer countTurns() {
        Integer count = 0;
        for (LinkedTreeMap elem : this.track) {
           if (elem.get("angle") != null) {
               count ++;
           }
        }
        return count;
    }
    
     private Integer countHarmlessTurns() {
        Integer count = 0;
        for (LinkedTreeMap elem : this.track) {
           if (elem.get("angle") != null) {
              if (this.canBeConsideredHarmlessDirect(this.track.indexOf(elem))) {
                  count ++;
              }
           }
        }
        return count;
    }
    
    private LinkedTreeMap getMapPiece(int Idx) {
        if (this.mapping.get(Idx) != null ) {
            return this.track.get(this.mapping.get(Idx));
        } else {
            return this.track.get(Idx);
        }
    }
    
    private Boolean shouldWeSwitchLane(int idx, int currentLaneId) {
        
        Integer nextTurnDirection =  this.getDirectionOfNextTurn(idx);
        Integer currentLane = currentLaneId;
        Integer idealLane = this.getTheIdealLane();
        
        if (isThereTurnCombo() && nextTurnDirection != getNextThreeDirectionAvarage(idx) && currentLane != idealLane){
            return true;
        }
        
        return false;
    }
    
    private Integer getTheIdealLane () {
        //TODO: Get the switch location and get what's inside
        
        return 0;
    }
    
    private boolean isThereTurnCombo (){
        //TODO: Get the switch location and return if there's 3 turn without switch
        
        return false;
    }
    
    private Integer getNextThreeDirectionAvarage(int idx) {
        int dir;
        int dir2;
        int dir3;
        Integer j = idx + 1;
        Boolean found = false;
        do {            
            j++;
            if (this.getMapPiece(j).get("angle") != null) {
                found = true;
            }
        } while (j < this.track.size() && this.getMapPiece(j).get("angle") == null);
        
        if (found) {
            if ((Double)this.getMapPiece(j).get("angle") < 0) {
                dir = 0;
            }  else {
                dir = 1;
            }
        } else {
            dir = 2;
        }
        
        
        Integer k = j + 1;
        do {            
            k++;
            if (this.getMapPiece(k).get("angle") != null) {
                found = true;
            }
        } while (k < this.track.size() && this.getMapPiece(k).get("angle") == null);
        
        if (found) {
            if ((Double)this.getMapPiece(k).get("angle") < 0) {
                dir2 = 0;
            }  else {
                dir2 = 1;
            }
        } else {
            dir2 = 2;
        }
        
        Integer l = k + 1;
        do {            
            l++;
            if (this.getMapPiece(l).get("angle") != null) {
                found = true;
            }
        } while (l < this.track.size() && this.getMapPiece(l).get("angle") == null);
        
        if (found) {
            if ((Double)this.getMapPiece(l).get("angle") < 0) {
                dir3 = 0;
            }  else {
                dir3 = 1;
            }
        } else {
            dir3 = 2;
        }
        
        
        if (dir == dir2){
            return dir;
        } else if (dir == dir3){
            return dir;
        } else {
            dir = dir2;
            return dir;
        }
    }
    
    //Returns 1 on Left turn, returns 0 on right, 2 on no more turns found.
    private Integer getDirectionOfNextTurn(int idx) {
        Integer j = idx + 1;
        Boolean found = false;
         try{
            do {            
                if (this.getMapPiece(j).get("angle") != null) {
                    found = true;
                    break;
                } 
                j++;
            } while (!found);
        } catch (Exception ignored) {
                return 2;
            }
        if (found) {
            if ((Double)this.getMapPiece(j).get("angle") < 0) {
                return 0;
            }  else {
                return 1;
            }
        } else {
        return 2;
        }
    }
}
